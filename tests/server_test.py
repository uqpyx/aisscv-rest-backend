import unittest
from unittest.mock import patch
import sys
from flask import Flask
from flask.testing import FlaskClient

sys.path.append("../")

from server import app


class AppTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def tearDown(self):
        pass

    def test_instruction_with_existing_state(self):
        response = self.app.get("/instruction?state=state_1")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data, b"Insert blue_long into the "
        )  # Replace with the expected instruction

    def test_instruction_with_nonexistent_state(self):
        response = self.app.get("/instruction?state=state_20")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, b"No instruction found for the given state.")

    def test_instruction_with_error(self):
        # Simulate an error during instruction retrieval
        with unittest.mock.patch("server.sqlite3.connect") as mock_connect:
            mock_connect.side_effect = Exception("Simulated exception")
            response = self.app.get("/instruction?state=state_1")

        self.assertEqual(response.status_code, 500)
        self.assertEqual(response.data, b"Error occurred while retrieving instruction.")


if __name__ == "__main__":
    unittest.main()
