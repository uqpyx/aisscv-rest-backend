from flask import Flask, request, jsonify, make_response
import sqlite3
import os
import json
from flask_cors import CORS

# Determine paths
current_directory = os.path.dirname(os.path.abspath(__file__))
database_file = os.path.join(current_directory, "lego.db")

# Initialize Flask app
app = Flask(__name__)
app.config["JSONIFY_PRETTYPRINT_REGULAR"] = False
CORS(app)


@app.route("/")
def main():
    """Root Endpoint: Returns a welcome message."""
    return "Welcome to the AISCV CV LEGO server!"


@app.route("/instruction", methods=["GET"])
def instruction():
    """Endpoint to retrieve instructions based on the current state and mode."""
    current_state = request.args.get("state")
    current_state_num = int(current_state.split("_")[1])
    assemblyMode = request.args.get("assemblyMode")
    conn, cur = None, None
    try:
        conn = sqlite3.connect(database_file)
        cur = conn.cursor()

        # Determine next state and table to query based on assembly mode
        if assemblyMode == "assembly":
            next_state = current_state_num + 1
            query_table = "states_assembly"
        else:
            next_state = current_state_num
            query_table = "states_disassembly"

        # Retrieve instruction
        formatted_name = "state_" + str(next_state)
        cur.execute(
            f"SELECT instruction FROM {query_table} WHERE name=?", (formatted_name,)
        )
        instruction_query = cur.fetchone()

        # Retrieve parts associated with the state
        cur.execute(
            "SELECT part, quantity FROM states_parts WHERE state=?", (formatted_name,)
        )
        parts_query = cur.fetchall()
        parts = [{"part": part[0], "quantity": part[1]} for part in parts_query]

        if instruction_query:
            return make_response(
                jsonify({"instruction": instruction_query[0], "next_parts": parts}), 200
            )
        else:
            return make_response(
                "No instruction or next_parts found for the given state.", 400
            )

    except Exception as e:
        print("Error occurred while retrieving instruction:", str(e))
        return make_response("Error occurred while retrieving instruction.", 500)
    finally:
        if cur:
            cur.close()
        if conn:
            conn.close()


@app.route("/full-set-check", methods=["POST"])
def full_set_check():
    """Endpoint to compare the provided set against the full set of parts."""
    conn, cur = None, None
    try:
        input_data = json.loads(request.get_data().decode("utf-8"))
        conn = sqlite3.connect(database_file)
        cur = conn.cursor()

        cur.execute("SELECT * FROM parts")
        full_set = dict(cur.fetchall())
        result = full_set_calculator(input_data, full_set)
        input_json = convert_input_to_json(input_data)

        return make_response(jsonify({"check": result, "input": input_json}), 200)
    except Exception as e:
        print("Error occurred while retrieving parts:", str(e))
        return make_response("Error occurred while retrieving parts.", 500)
    finally:
        if cur:
            cur.close()
        if conn:
            conn.close()


@app.route("/state-part-check", methods=["POST", "GET"])
def state_set_check():
    """Endpoint to verify if the correct parts are present for a specific state."""
    conn, cur = None, None
    try:
        current_state_num = int(request.args.get("state").split("_")[1]) + 1
        input_data = json.loads(request.get_data().decode("utf-8"))

        conn = sqlite3.connect(database_file)
        cur = conn.cursor()

        formatted_name = "state_" + str(current_state_num)
        cur.execute(
            "SELECT part, quantity FROM states_parts WHERE state=?", (formatted_name,)
        )
        state_parts = dict(cur.fetchall())

        result = check_parts_of_states(input_data, state_parts)
        input_json = convert_input_to_json(input_data)

        return make_response(jsonify({"check": result, "input": input_json}), 200)
    except Exception as e:
        print("Error occurred while retrieving parts:", str(e))
        return make_response("Error occurred while retrieving parts.", 500)
    finally:
        if cur:
            cur.close()
        if conn:
            conn.close()


def full_set_calculator(input_data, full_set):
    """Function to check the provided input against the full set."""
    input_parts = {
        "engine": 0,
        "blue_short": 0,
        "blue_long": 0,
        "gray_45": 0,
        "gray_90": 0,
        "black_stick_short": 0,
        "black_short": 0,
        "red_shaft": 0,
        "gray_long": 0,
        "gray_short": 0,
        "green_3": 0,
        "gray_frame": 0,
        "white_45": 0,
        "wheel": 0,
        "tire": 0,
        "gray_loop": 0,
    }
    missing_parts = {}
    result = {}
    avg_confidence = 0
    parts_counter = 0

    for part in input_data:
        if part["class"] not in input_parts:
            continue
        input_parts[part["class"]] += 1
        avg_confidence += part["confidence"]
        parts_counter += 1

    if parts_counter != 0:
        avg_confidence /= parts_counter
    else:
        avg_confidence = 0

    for part in input_parts:
        missing_parts[part] = full_set[part] - input_parts[part]

    complete = True
    for part in missing_parts:
        if missing_parts[part] != 0:
            complete = False
            break

    result["complete"] = complete
    result["missing_parts"] = missing_parts
    result["avg_confidence"] = avg_confidence

    return result


def check_parts_of_states(input_data, state_parts):
    """Function to verify if the correct parts are present for a specific state."""
    missing_parts = {}
    result = {}
    avg_confidence = 0
    parts_counter = 0

    for part in input_data:
        if part["class"] not in state_parts:
            continue
        state_parts[part["class"]] -= 1
        avg_confidence += part["confidence"]
        parts_counter += 1

    if parts_counter != 0:
        avg_confidence /= parts_counter
    else:
        avg_confidence = 0

    complete = True
    for part in state_parts:
        if state_parts[part] != 0:
            complete = False
            break

    result["complete"] = complete
    result["missing_parts"] = state_parts
    result["avg_confidence"] = avg_confidence

    return result


def convert_input_to_json(data):
    """Utility function to convert input data to its equivalent JSON format."""
    parts = {
        "engine": 0,
        "blue_short": 0,
        "blue_long": 0,
        "gray_45": 0,
        "gray_90": 0,
        "black_stick_short": 0,
        "black_short": 0,
        "red_shaft": 0,
        "gray_long": 0,
        "gray_short": 0,
        "green_3": 0,
        "gray_frame": 0,
        "white_45": 0,
        "wheel": 0,
        "tire": 0,
        "gray_loop": 0,
        "state_8": 0,
        "state_11": 0,
        "state_12": 0,
        "state_13": 0,
        "state_14": 0,
    }

    # Iterate over the input list and update the counts in input_parts
    for item in data:
        part_class = item["class"]
        if part_class in parts:
            parts[part_class] += 1

    return parts


if __name__ == "__main__":
    app.run(debug=True)
